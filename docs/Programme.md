# Explication du programme

Le programme contient dans un premier temps les commande d'initialisation et de configuration des différents périphériques.
De plus, la centrale inertielle est configurée en mode fusion: elle va calculer l'accélération du au mouvement et celle du à la gravité suivant les différents axes.
La communication avec celle ci est tester durant cette phase d'initialisation. Dans le cas où la communication avec celle-ci est défaillante, un message "erreur de La Centrale" s'affichera sur l'écran LCD.

## Variable globales
Les variables globales sont :

int16_t gx=0,gy=0,gz=0,accx=0,accy=0,accz=0; accélération suivant les différentes coordonnées

int16_t r_b=10, r_t=15; raton de la bille et du trou
int16_t x_b,y_b,z_b,x_t,y_t,z_t; position de la bille et du trou

uint8_t etat_partie=1; activation ou desactivation du menu
uint16_t score=0; score
uint16_t difficulte=1; difficulté
int16_t frottement=1; frottement sec

static TS_StateTypeDef  TS_State; état de l'écran

## Les tâches
### Tâche position

Cette tâche lance les fonction qui permette d'actualisé les variables gx,gy,gz,accx,accy et accz.
A partir de ces nouvelles données, la tâche va calculer la nouvelle position de la bille, en tenant compte des conditions de bord.

De plus cette tache vérifié si la nouvelle position implique que la bille soit rentrée dans le trou.
Dans ce cas, la tâche active la tâche Partie_init.
### Tâche affichage_bille

Cette tâche efface l'ancienne position de la bille, réacffiche le trou et enfin affiche la bille à sa nouvelle position.

### Tâche Partie_init

Cette tâche va desactiver la Tâche position et la Tâche affichage_bille.
Elle va ensuite générer aléatoirement les variables de position de la bille et du trou.
Enfin, elle réactive la Tâche position et la Tâche affichage_bille puis s'auto suspend.

## Les difficultés rencontrées
+ La première difficulté rencontrée a été de communiquer avec la centralle inertielle.

+ Ensuite il a fallut mettre en place un actualisation de la position et géré les effets de bords.
En effet si l'écran doit affiché quelque chose en dehors de sa capacité, cela va déclancher une erreur.

+ Il a fallut aussi créer un sensation physiquement acceptable avec des rebonds sur les rebords de l'écran ainsi que des frotements.

+ Enfin il a fallut mettre en place une gestion du lancement et de fin de partie

## Les solutions développées
+ J'ai crée 2 fonction qui me permette de récupérer l'accélération et la gravité depuis la centrale inertielle. Ces fonctions sont basées sur des fonctions préexistantes sur STM32.

+ J'ai opté pour la création de 2 tache pour gérer la position et l'affichage de la bille

+ Une 3ème tâche a été créée, elle sert de commande et gère le lancement des 2 autres tâches

## Schéma d'interaction des tâches

![illustration](./img/schema_tache.png)
