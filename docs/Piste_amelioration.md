# Amélioration

Ce projet peut encore être amélioré :

+ Le design du menu est très sobre et mériterais des améliorations.

+ Il serait possible de généré un labyrinthe. (prévoir du temps)

+ Possibilité d'implémenter une compétition avec une gestion du temps

+ Gestion de la difficulté en modifiant les frottements ou le coefficiant appliqué à l'accélération.
