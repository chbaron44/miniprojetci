# Guide d'utilisation
Une fois les fichiers de code téléversé sur la carte, il est possible de lancer une partie.

Lors du lancement du jeu, une page de menu s'affiche : Il suffit de suivre les indications sur celle-ci pour lancer une manche.
L'objectif de la manche est très simple : il faut faire rentrer la bille rouge dans le trou noir.
Pour cela il faut incliner la carte et laisser faire la gravité !
Une fois cette objectif accomplit un écran des score s'affichera. Si vous souhaiter relancer une nouvelle partie, il faut à nouveau appuyer sur l'écran.
