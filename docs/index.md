# Mini projet : jeu de la bille

L'objectif de ce mini projet est de recréer une version simplifiée d'un jeu auquel vous avez probablement joué durant votre enfance : "Le jeux de la bille". Il faut faire rentré une bille dans un trou se situant sur une petite plateforme ne inclinant celle ci. Voici une image qui vous aidera certainement à mieux comprendre de quoi je parle :

![illustration](./img/image_jeu_bille.jpg)

Les fichiers de code du mini-projet sont disponibles à l'adresse suivante : https://gitlab.com/chbaron44/miniprojetci.git

##ProblématiqueS apportées par le mini-projet
Ce mini projet pose un premier problème : obtenir l'accélération de la bille pour pouvoir en déduire sa trajectoire.
Les autres problématiques ont déjà été abordée lors des tps précédents : il faut gérer l'affichage de la bille et du trou en temps réel, calculer si la bille est rentrée dans le trou. De plus il serait bien de rajouter une petite interface de comptage des points/ lancement des parties.
De plus ce mini projet devra s'accompagner d'un site internet, et le code sera versionner par git sur GitLab.
